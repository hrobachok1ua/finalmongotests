const MongoClient = require("mongodb").MongoClient;
   
const Joi = require('joi')
const express = require('express')
const app = express()

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/');
    },
    filename: function(req, file, cb){
        cb(null, file.originalname);
    }
})
const upload = multer({storage: storage})

app.use(express.json())

app.use(express.static("/"))

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

   // res.setHeader('Content-Type', 'application/json');

    // Pass to next layer of middleware
    next();
});

let db;

app.get('/', (req, res) => {
    db.collection('users').find().toArray((err, docs)=>{
        if(err) {
            console.log(err)
            return res.sendStatus(500)
        }
        res.send(docs);
    })
})

app.get('/uploads/profilePage.jpg', (req, res) => {
        res.sendFile("C:/Users/bonuftc/finalVersion/backend&db/mongo/uploads/profilePage.jpg");
})

app.get('/details/:id', (req, res) => {
    let user;
    db.collection('users').find().toArray((err, docs)=>{
        if(err) {
            console.log(err)
            return res.sendStatus(500)
        }
        //console.log(typeof docs)
        user = docs.find(e => {
            return ""+e['_id'] === req.params.id
        })
        if(!user) return res.status(404).send('Course wasn\'t found')
        else res.send(user);        
    })
})

app.post('/create',upload.single('image')  ,(req, res) => {
    console.log('Called')
    const schema = {
        title: Joi.string().min(1).required(),
        author: Joi.string().min(1).required(),
        date: Joi.string().min(1).required(),
        description: Joi.string().min(1).required(),
        img: Joi.string().min(1).required(),
        keyCode: Joi.number().min(1).required(),
        post: Joi.boolean().required(),
        type: Joi.string().min(1).required()
    }

    const result = Joi.validate(req.body, schema)

    if(result.error) {
        return res.status(400).send(result.error.details[0].message)
    }
    
    // const user = {
    //     author: req.file.author,
    //     date: req.file.date,
    //     description: req.file.description,
    //     img: "http://localhost:4100/uploads/"+req.file.originalname,
    //     keyCode: req.file.keyCode,
    //     post: req.file.post,
    //     title: req.file.title,
    //     type: req.file.type
    // }
    // console.log(user)
    const user = {...req.body}
    db.collection('users').insertOne(user, function(err, result){
        if(err) {
            return res.sendStatus(500)
        }
    })
    res.send(user)
})

const port = process.env.port || 4100

const url = "mongodb://localhost:27017/";

const mongoClient = new MongoClient(url, { useNewUrlParser: true });
 
mongoClient.connect(function(err, client){
      
    db = client.db("usersdb");
    if(err){ 
        return console.log(err);
    }
    let user = {author: 'Anonim', date: "2019-02-21", description: "dfg", img: "https://www.gstatic.com/webp/gallery3/1.sm.png", 
    keyCode: 28983, post: true, title: "fdg", type: "News"};
    app.listen(port, () => {
        //db.collection("users").drop()
        db.collection("users").insertOne(user, function(err, result){
          
            if(err){ 
                return console.log(err);
            }
        });
        console.log(`Listening onds port ${port}`);
    })
});
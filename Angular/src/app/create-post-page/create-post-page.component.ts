import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostsAndUsersService } from '../posts-and-users.service';
import { Router } from '@angular/router';
import { ExitGuard } from '../exit.guard';
import { Observable } from 'rxjs';

interface postObject {
  author: string,
  date: string,
  description: string,
  img: string,
  key: string,
  keyCode: number,
  post: boolean,
  title: string,
  type: string
}

@Component({
  selector: 'app-create-post-page',
  templateUrl: './create-post-page.component.html',
  styleUrls: ['./create-post-page.component.scss']
})
export class CreatePostPageComponent implements OnInit, OnDestroy {

  swModal = false;

  posts;

  type = 'News';
  post = true;
  author = '';
  keyCode = Math.floor(Math.random() * 999999);

  description = '';
  changeUrl = false;
  imgFile: File = null;

  constructor(private postsAndUsersService: PostsAndUsersService, private r: Router) { }

  AddImgFile(event) {
    this.imgFile = <File>event.target.files[0]
  }

  ngOnInit() {
    if (this.postsAndUsersService.userEmail === '') {
      this.author = 'Anonim';
    } else {
      this.author = this.postsAndUsersService.userEmail;
    }

    this.postsAndUsersService.getPostsNoSend()
    .subscribe(post => {
      this.posts = post.sort((a, b) => {
        if (a.date > b.date) {
          return 1;
        }
        if (a.date < b.date) {
          return -1;
        }
        return 0;
      });
    });
  }

  showModal() {
    this.swModal = true;
  }
  hideModal() {
    this.swModal = false;
  }

  createPost(v) {
    //let newFormValue = {...v}
    // const fd = new FormData();
    // fd.append('image', this.imgFile, this.imgFile.name)
    // newFormValue.img = fd;
    // fd.append('date', v.date)
    // fd.append('description', v.description)
    // fd.append('key', v.key)
    // fd.append('keyCode', v.keyCode)
    // fd.append('post', v.post)
    // fd.append('title', v.title)
    // fd.append('author', v.author)
    // fd.append('type', v.type)

    this.changeUrl = true;
    this.postsAndUsersService.sendPost(v)
    this.r.navigate(['/']);  
    //this.postsAndUsersService.mongoPosts.push(newFormValue);
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (this.description !== '' && !this.changeUrl) {
      return confirm('Your data in description won\'t save');
    } else {
        return true;
    }
  }

  ngOnDestroy() {
    this.changeUrl = false;
  }
}

import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http'

interface postObject {
  author: string,
  date: string,
  description: string,
  img: string,
  key: string,
  keyCode: number,
  post: boolean,
  title: string,
  type: string
}

@Injectable({
  providedIn: 'root'
})
export class PostsAndUsersService {

  postRef: AngularFireList<any>;
  posts: Observable<any[]>;

  userEmail = '';
  admin = false;
  checkEmail = false;
  deletedAcc = false;
  alreadyReg = false;
  changed = false;
  deletedArr = [];
  shouldRender = false;

  showDeletedBin = false;
  onDetail = false;
  url = 'http://localhost:4100'

  mongoPosts = [];

  constructor(private http: AngularFireDatabase, private r: Router, private activateRoute: ActivatedRoute, private ownHttp: HttpClient) {
    this.getPosts().subscribe(posts => {
      this.mongoPosts = posts;
    })
  }

  signInUser(email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(resp => {
      console.log(resp);
      this.userEmail = resp.user.email; console.log(this.userEmail);

      if (this.userEmail === 'hrobachok1ua@gmail.com') {
        this.admin = true;
      }

      this.r.navigate(['/']);
    }).catch(e => console.log(e.message));
  }

  signUpUser(email, password) {
    this.userEmail = '';
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(resp => {
      firebase.auth().currentUser.sendEmailVerification()
      .then(() => {
        this.checkEmail = true;
        console.log('check email');
      });
      this.userEmail = resp.user.email; console.log(this.userEmail);

      if (this.userEmail === 'hrobachok1ua@gmail.com') {
        this.admin = true;
      }

      this.r.navigate(['/']);
    })
    .catch(e => {
      this.alreadyReg = true;
    });
  }

  getPosts() {
    let pageQuantityQueryParam
    this.activateRoute.queryParams.subscribe(posts => {
      pageQuantityQueryParam = +posts.loadQuantity
    });
    return this.ownHttp.get(this.url).pipe(
      map(post => {
        return (<postObject[]>post).map(el => {
          let oldPost = {...el}
          let k = oldPost['_id']
          oldPost.key = k
          delete oldPost['_id']
          return oldPost
        }).slice(0, pageQuantityQueryParam)
      })
    )
    // this.postRef = this.http.list('/posts', ref => {
    //   return ref.limitToLast(q);
    // });
    // return this.posts = this.postRef.snapshotChanges().pipe(
    //   map(changes => {
    //     return changes.map(c => ({ key: c.payload.key, ...c.payload.val()
    //     }));
    //   })
    // );
  }
  getPostsNoSend() {
    let pageQuantityQueryParam;
    this.activateRoute.queryParams.subscribe(posts => {
      pageQuantityQueryParam = +posts.loadQuantity
    })
    return this.ownHttp.get(this.url).pipe(
      map(post => {
        return (<postObject[]>post).filter(el => !el.post).map(el => {
          let oldPost = {...el}
          let k = oldPost['_id']
          oldPost.key = k
          delete oldPost['_id']
          return oldPost
        }).slice(0, pageQuantityQueryParam)
      })
    )
    // this.postRef = this.http.list('/posts', ref => {
    //   return ref.limitToLast(q);
    // });
    // return this.posts = this.postRef.snapshotChanges().pipe(
    //   map(changes => {
    //     return changes.filter(el => !el.payload.val().post)
    //     .map(c => ({ key: c.payload.key, ...c.payload.val()
    //     }));
    //   })
    // );
  }
  getPostsOnlySend() {
    let pageQuantityQueryParam;
    this.activateRoute.queryParams.subscribe(posts => {
      pageQuantityQueryParam = +posts.loadQuantity
    })
    return this.ownHttp.get(this.url).pipe(
      map(post => {
        return (<postObject[]>post).filter(el => el.post).map(el => {
          let oldPost = {...el}
          let k = oldPost['_id']
          oldPost.key = k
          delete oldPost['_id']
          return oldPost
        }).slice(0, pageQuantityQueryParam)
      })
    )
    // this.postRef = this.http.list('/posts', ref => {
    //   return ref.limitToLast(q);
    // });
    // return this.posts = this.postRef.snapshotChanges().pipe(
    //   map(changes => {
    //     return changes.filter(el => el.payload.val().post)
    //     .map(c => ({ key: c.payload.key, ...c.payload.val()
    //     }));
    //   })
    // );
  }
  sendPost(formValue) {
    // return this.http.list('/posts')
    //   .push(v);
    return this.ownHttp.post(this.url+"/create",formValue, {
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress){
        console.log("Upload progress: "+HttpEventType.Response);
      } else if (event.type === HttpEventType.Response){
        console.log(event)
      }
    })
  }
  // deletePost(i) {
  //   return this.http.list('/posts').remove(i.key);
  // }
}

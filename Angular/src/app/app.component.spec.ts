import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { PostsAndUsersService } from './posts-and-users.service';
import { AppRoutingModule } from './app-routing.module';
import { HomePageComponent  } from './home-page/home-page.component'
import { PostComponent } from './home-page/post/post.component'
import { Routes, RouterModule } from '@angular/router';
import { RegisterPageComponent } from './register-page/register-page.component'
import { CreatePostPageComponent } from './create-post-page/create-post-page.component'
import { UserPageComponent } from './user-page/user-page.component'
import { PostPageComponent } from './home-page/post-page/post-page.component'
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AppRoutingModule,
        RouterModule,
        MatCardModule,
        MatButtonModule,
        FormsModule,
        AngularFireModule.initializeApp({
          apiKey: 'AIzaSyDzw8sASA8nm1_bNh8Wt5CfVz93AoCbHCU',
          authDomain: 'postapp-f507f.firebaseapp.com',
          databaseURL: 'https://postapp-f507f.firebaseio.com',
          projectId: 'postapp-f507f',
          storageBucket: 'postapp-f507f.appspot.com',
          messagingSenderId: '75333077798'
        }),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        BrowserAnimationsModule,
        HttpClientModule
      ],
      declarations: [
        AppComponent,
        HomePageComponent,
        PostComponent,
        RegisterPageComponent,
        CreatePostPageComponent,
        UserPageComponent,
        PostPageComponent
      ],
      providers: [
        PostsAndUsersService
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    expect(app).toBeTruthy();
  });

  it('should contain userEmail', fakeAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    let compiled = fixture.debugElement.nativeElement;
    tick()
    $PostsAndUsersService.userEmail = ""
    //expect(compiled.querySelectorAll('button')[1].textContent).toContain("");
    expect($PostsAndUsersService.userEmail).toBe("");
  }))
  it('should display name when logged in', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const $PostsAndUsersService = fixture.debugElement.injector.get(PostsAndUsersService);
    let compiled = fixture.debugElement.nativeElement;

    fixture.detectChanges();

    if ($PostsAndUsersService.userEmail !== '') {
      expect(compiled.querySelectorAll('button')[1].textContent).toContain($PostsAndUsersService.userEmail);
    }
  })

});
